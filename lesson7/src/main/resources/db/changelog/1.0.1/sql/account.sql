create sequence if not exists account_seq start 1;

CREATE TABLE if not exists account
(
    user_id  bigint      not null default nextval('account_seq' :: regclass),
    username VARCHAR(50) NOT NULL,
    password text        NOT NULL,
    country  VARCHAR(355),
    role_id int
    );

alter table account
    add constraint account_pk primary key (user_id);

alter table account
    add constraint account_username_uq unique (username);