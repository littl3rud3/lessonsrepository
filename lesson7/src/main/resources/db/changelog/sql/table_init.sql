insert into person(first_name, last_name, middle_name, birth_date, gender) VALUES ('Дмитрий', 'Клименко', 'Сергеевич', '25-06-2000','М');
insert into person(first_name, last_name, middle_name, birth_date, gender) VALUES ('Александр', 'Корнеев', 'Юрьевич', '16-07-2000','М');
insert into person(first_name, last_name, middle_name, birth_date, gender) VALUES ('Жирова', 'Анастасия', 'Олеговна', '29-08-2000','Ж');
insert into employment VALUES
(1,1, '10-01-2020','01-01-2021','1','Яндекс','Москва','Техник',1);
insert into employment values
(1,1, '01-01-2020','01-01-2021','1','Гугл','Москва','Техник',2);
insert into employment VALUES
(1,1, '01-01-2020','01-01-2021','1','Мэил','Москва','Техник',3);
insert into employment VALUES
(2,1, '10-01-2020','01-01-2021','1','Гугл','Москва','Техник',1);
insert into employment VALUES
(3,1, '04-10-2020','04-12-2020','1','ООО ЭйТи Консальтинг','Москва','Разработчик',1);