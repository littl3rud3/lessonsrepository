package com.example.exampleliqubase.dao;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.example.exampleliqubase.model.EmploymentEntity;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface EmploymentMapper {
    int deleteByPrimaryKey(Long employmentId);

    int insert(EmploymentEntity record);

    int insertSelective(EmploymentEntity record);

    EmploymentEntity selectByPrimaryKey(Long employmentId);

    int updateByPrimaryKeySelective(EmploymentEntity record);

    int updateByPrimaryKey(EmploymentEntity record);

    List<EmploymentEntity> getByPersonId(Long personId);

    void insertAll(@Param("list") List<EmploymentEntity> employmentsToSave);

    void updateAll(@Param("list") List<EmploymentEntity> employmentsToUpdate);

    void deleteByIds(@Param("ids") Set<Long> ids);

    @Delete("DELETE FROM employment WHERE person_id = #{personId} AND employment_id = #{employmentId}")
    void deleteById(EmploymentEntity employmentEntity);

    @Select("SELECT * FROM employment WHERE person_id = #{personId} AND employment_id = #{employmentId}")
    EmploymentEntity findById(Long personId, Long employmentId);

    @Update("UPDATE employment SET start_dt =#{startDt}, end_dt = #{endDt}, version = #{version}, organization_name = #{organizationName}, " +
            "position_name = #{positionName}, person_id = #{personId}, work_type_id = null, organization_address = null " +
            "WHERE employment_id = #{employmentId} AND person_id = #{personId} ")
    void update(EmploymentEntity employmentEntity);

    @Delete("DELETE FROM employment WHERE person_id = #{personId}")
    void deleteAll(Long personId);

}