package com.example.exampleliqubase.dao;

import java.util.Optional;

import com.example.exampleliqubase.model.Account;

public interface AccountMapper {
    int deleteByPrimaryKey(Long userId);

    int insert(Account record);

    int insertSelective(Account record);

    Account selectByPrimaryKey(Long userId);

    int updateByPrimaryKeySelective(Account record);

    int updateByPrimaryKey(Account record);

    Optional<Account> getByLogin(String login);

}