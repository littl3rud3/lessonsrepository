package com.example.exampleliqubase.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.example.exampleliqubase.dao.AccountMapper;
import com.example.exampleliqubase.dao.PermissionMapper;
import com.example.exampleliqubase.model.Account;


import com.example.exampleliqubase.model.PermissionEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Account account = accountMapper.getByLogin(login)
                .orElseThrow(() -> new UsernameNotFoundException("user not found"));

        UserDetails user = User.builder()
                .username(account.getUsername())
                .password(account.getPassword())
                .authorities(getAuthorities(account.getRoleId()))
                .build();
        return user;
    }

    private List<GrantedAuthority> getAuthorities(int roleId) {

        return permissionMapper.findPermissions(roleId).stream().map(x -> new SimpleGrantedAuthority(x)).collect(Collectors.toList());
//        return Collections.singletonList(new SimpleGrantedAuthority("ROLE_EXAMPLE"));
    }
}

