package com.example.exampleliqubase.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.example.exampleliqubase.api.EmploymentService;
import com.example.exampleliqubase.dao.EmploymentMapper;
import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.model.EmploymentEntity;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmploymentServiceImpl implements EmploymentService {

    List<EmploymentEntity> toBeAdded = new ArrayList<>();

    @Autowired
    private EmploymentMapper employmentMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void saveEmployment(EmploymentDTO employmentDTO) {
        EmploymentEntity employmentEntity = modelMapper.map(employmentDTO, EmploymentEntity.class);
        employmentMapper.insert(employmentEntity);
    }

    @Override
    public void updateRecords(List<EmploymentDTO> employmentsDTO, Long personId) {

        employmentsDTO.removeIf(x -> x.getPersonId() != personId);

        List<EmploymentEntity> employmentEntities = employmentMapper.getByPersonId(personId);

        List<EmploymentDTO> newOne = employmentsDTO.stream().filter(x -> x.getEmploymentId() == null).collect(Collectors.toList());

        List<EmploymentDTO> saveOrModify = employmentsDTO.stream().filter(x -> x.getEmploymentId() != null).collect(Collectors.toList());

        List<Long> employmentIds = employmentEntities.stream().map(x -> x.getEmploymentId()).collect(Collectors.toList());

        newOne.stream().forEach(x -> x.setVersion(1));

        for (EmploymentDTO e : saveOrModify) {
            if (employmentIds.contains(e.getEmploymentId())) {
                modify(e);
            } else save(e);
        }
        employmentMapper.deleteAll(personId);
        employmentMapper.insertAll(toBeAdded);
        newOne.stream().map(x -> modelMapper.map(x, EmploymentEntity.class)).forEach(y -> employmentMapper.insert(y));

        toBeAdded.clear();

    }

    @Override
    public void save(EmploymentDTO employmentDTO) {
        EmploymentEntity employmentEntity = modelMapper.map(employmentDTO,EmploymentEntity.class);
        employmentEntity.setVersion(1);
       toBeAdded.add(employmentEntity);
    }

    @Override
    public void modify(EmploymentDTO employmentDTO) {
        EmploymentEntity employmentEntity = modelMapper.map(employmentDTO, EmploymentEntity.class);
        if (employmentEntity.equals(modelMapper.map(employmentMapper.findById(employmentDTO.getEmploymentId(),employmentDTO.getPersonId()), EmploymentEntity.class)))
        return;
        employmentEntity.setVersion(employmentMapper.findById(employmentDTO.getEmploymentId(),employmentDTO.getPersonId()).getVersion()+1);
        toBeAdded.add(employmentEntity);
    }
}
