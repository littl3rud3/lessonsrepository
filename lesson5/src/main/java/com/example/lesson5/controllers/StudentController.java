package com.example.lesson5.controllers;

import com.example.lesson5.models.Student;
import com.example.lesson5.models.Teacher;
import com.example.lesson5.models.students_teachers;
import com.example.lesson5.services.StudentService;
import com.example.lesson5.services.stService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private stService stservice;

    @GetMapping()
    public List<Student> showAll() {
        return studentService.allStudents();
    }

    @PostMapping("/add")
    public void addStudent(@RequestBody Student student) {
        studentService.addStudent(student);
    }

    @DeleteMapping("/{id}/delete")
    public void removeStudent(@PathVariable int id) {
        studentService.deleteStudent(studentService.findById(id));
    }

    @GetMapping("{id}")
    public Student showOne(@PathVariable int id) throws Exception {
        return studentService.findById(id);
    }

    @PostMapping("/{id}/add-lesson")
    public void addLesson(@PathVariable int id, @RequestBody students_teachers st) {
        stservice.addNew(id, st.getId_teacher());
    }

    @DeleteMapping("/{id}/remove-lesson")
    public void removeLesson(@PathVariable int id, @RequestBody students_teachers st) {
        st.setId_student(id);
        stservice.remove(st);
    }

    @GetMapping("/{id}/all-lessons")
    public List<Teacher> allLessons(@PathVariable int id) {
        if (studentService.findById(id) != null)
        return stservice.allTeachers(id);
        else throw new NoSuchElementException();
    }

    @ExceptionHandler(NoSuchElementException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public HashMap<String, String> handleWrongId(Exception e) {
        HashMap<String, String> response = new HashMap<>();
        response.put("message", "There is no student with such id");
        response.put("error", e.getClass().getSimpleName());
        return response;
    }

    @ExceptionHandler(org.postgresql.util.PSQLException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public HashMap<String, String> handleWrongParameter(Exception e) {
        HashMap<String, String> response = new HashMap<>();
        response.put("message", "There is something wrong in your parameters");
        response.put("error", e.getClass().getSimpleName());
        return response;
    }
}
