package com.example.lesson5.controllers;


import com.example.lesson5.models.Student;
import com.example.lesson5.models.Teacher;
import com.example.lesson5.models.students_teachers;
import com.example.lesson5.services.TeacherService;
import com.example.lesson5.services.stService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/teachers")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private stService stservice;

    @GetMapping()
    public List<Teacher> showAll(){
        return teacherService.allTeachers();
    }

    @PostMapping("/add")
    public void addTeacher(@RequestBody Teacher teacher){
        teacherService.addTeacher(teacher);
    }

    @DeleteMapping("/{id}/delete")
    public void removeTeacher(@PathVariable int id) {
        teacherService.deleteTeacher(teacherService.findById(id));
    }

    @GetMapping("{id}")
    public Teacher showOne(@PathVariable int id) {
        return teacherService.findById(id);
    }

    @PostMapping("/{id}/add-student")
    public void addLesson(@PathVariable int id, @RequestBody students_teachers st) {
        st.setId_teacher(id);
        stservice.addNew(st.getId_student(),id);
    }

    @DeleteMapping("/{id}/remove-student")
    public void removeLesson(@PathVariable int id, @RequestBody students_teachers st) {
        st.setId_teacher(id);
        stservice.remove(st);
    }

    @GetMapping("/{id}/all-students")
    public List<Student> allStudents(@PathVariable int id) {
        return stservice.allStudents(id);
    }

    @ExceptionHandler(NoSuchElementException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public HashMap<String, String> handleWrongId(Exception e) {
        HashMap<String, String> response = new HashMap<>();
        response.put("message", "There is no teacher with such id");
        response.put("error", e.getClass().getSimpleName());
        return response;
    }

    @ExceptionHandler(org.postgresql.util.PSQLException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public HashMap<String, String> handleWrongParameter(Exception e) {
        HashMap<String, String> response = new HashMap<>();
        response.put("message", "There is something wrong in your parameters");
        response.put("error", e.getClass().getSimpleName());
        return response;
    }
}
