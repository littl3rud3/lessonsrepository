package com.example.lesson5.repositories;

import com.example.lesson5.models.students_teachers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface stRepo extends JpaRepository<students_teachers, Integer> {
    @Query("SELECT st.id_teacher FROM students_teachers st WHERE st.id_student = ?1")
    List<Integer> findById_student(int id);

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO students_teachers(id_student, id_teacher) VALUES (?1,?2)", nativeQuery = true)
    void save(int id_student, int id_teacher);

    @Modifying
    @Transactional
    @Query("DELETE FROM students_teachers WHERE id_teacher = ?1")
    void delete(int id_student);

    @Query("SELECT st.id_student FROM students_teachers st WHERE st.id_teacher = ?1")
    List<Integer> findById_teacher(int id);


}
