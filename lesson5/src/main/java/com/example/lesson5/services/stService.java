package com.example.lesson5.services;

import com.example.lesson5.models.Constraint;
import com.example.lesson5.models.Student;
import com.example.lesson5.models.Teacher;
import com.example.lesson5.models.students_teachers;
import com.example.lesson5.repositories.stRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Service
public class stService {

    @Autowired
    private stRepo strepo;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private EntityManager entityManager;

    public void addNew(int id_student, int id_teacher){
        strepo.save(id_student, id_teacher);
    }

    public void remove(students_teachers st) {
        strepo.delete(st.getId_teacher());
    }

    public List<Teacher> allTeachers(int id) {
        List<Teacher> res = new ArrayList<>();
       List<Integer> v = strepo.findById_student(id);
       for (Integer i : v) {
           res.add(teacherService.findById(i));
       }
       return res;
    }

    public List<Student> allStudents(int id) {
        List<Student> res = new ArrayList<>();
        List<Integer> v = strepo.findById_teacher(id);
        for (Integer i : v) {
            res.add(studentService.findById(i));
        }
        return res;
    }
}
