package com.example.lesson5.services;

import com.example.lesson5.models.Student;
import com.example.lesson5.models.students_teachers;
import com.example.lesson5.repositories.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepo studentRepo;

    @Autowired
    private stService stServ;

    public List<Student> allStudents() {
        return studentRepo.findAll();
    }

    public Student findById(int id) {
        return studentRepo.findById(id).get();
    }

    public void addStudent(Student student) {
        studentRepo.save(student);
    }

    public void deleteStudent(Student student) {
        studentRepo.delete(student);
    }



//    public void addLesson(int id, Pupil pupil) {
//        Pupil p = pupilRepo.findAll().get(id-1);
//        p.setTeacher_id(pupil.getTeacher_id());
//        addPupil(p);
//    }

//    public List<Teacher> allLessons(int id) {
//        return teacherService.allTeachers().stream().filter(x -> x.getPupil_id()  == id).collect(Collectors.toList());
//    }

}
