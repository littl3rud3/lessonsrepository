package com.example.lesson5.services;

import com.example.lesson5.models.Teacher;
import com.example.lesson5.models.students_teachers;
import com.example.lesson5.repositories.TeacherRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {

    @Autowired
    private TeacherRepo teacherRepo;

    @Autowired
    private stService stServ;

    public List<Teacher> allTeachers() {
        return teacherRepo.findAll();
    }

    public Teacher findById(int id) {
        return teacherRepo.findById(id).get();
    }

    public void addTeacher(Teacher teacher) {
        teacherRepo.save(teacher);
    }

    public void deleteTeacher(Teacher teacher) {
        teacherRepo.delete(teacher);
    }


}
