package com.example.lesson5.models;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Objects;

@Component
@Data
public class Constraint implements Serializable {
    protected Integer id_student;
    protected Integer id_teacher;

    public Constraint() {}

    public Constraint(Integer id_student, Integer id_teacher) {
        this.id_student = id_student;
        this.id_teacher = id_teacher;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Constraint that = (Constraint) o;
        return Objects.equals(id_student, that.id_student) && Objects.equals(id_teacher, that.id_teacher);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id_student, id_teacher);
    }
}