package com.example.lesson5.models;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "students_teachers", schema = "public")
//@IdClass(Constraint.class)
public class students_teachers {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Constraint constraint;

    private int id_student;

    private int id_teacher;
}

