CREATE table students(
                         id SERIAL PRIMARY KEY ,
                         first_name varchar(30) NOT NULL,
                         last_name varchar(30) NOT NULL,
                         CHECK (length(first_name) > 2) ,
                         CHECK (length(last_name) > 3)

);
CREATE table students_teachers(
                                  id_student int,
                                  id_teacher int

);
CREATE table teachers(
                         id SERIAL PRIMARY KEY,
                         first_name varchar(30) NOT NULL ,
                         last_name varchar(30) NOT NULL,
                         subject varchar(50) NOT NULL,
                         CHECK (length(first_name) > 2) ,
                         CHECK (length(last_name) > 3),
                         CHECK (length(subject) > 2)
);
ALTER TABLE students_teachers ADD FOREIGN KEY (id_teacher) REFERENCES teachers (id) ON DELETE CASCADE;
ALTER TABLE students_teachers ADD FOREIGN KEY (id_student) REFERENCES students (id) ON DELETE CASCADE;
ALTER TABLE students_teachers ADD PRIMARY KEY (id_student, id_teacher);
INSERT INTO students(first_name, last_name) VALUES ('Руслан','Денисов');
INSERT INTO students(first_name, last_name) VALUES ('Идар','Бжаумыхов');
INSERT INTO students(first_name, last_name) VALUES ('Саба','Джолохава');
INSERT INTO students(first_name, last_name) VALUES ('Паруйр','Казарян');
INSERT INTO students(first_name, last_name) VALUES ('Анастасия','Жирова');
INSERT INTO teachers(first_name, last_name, subject) VALUES ('Елена', 'Царькова', 'Математика');
INSERT INTO teachers(first_name, last_name, subject) VALUES ('Наталья', 'Павлова', 'Русский язык');
INSERT INTO teachers(first_name, last_name, subject) VALUES ('Дмитрий', 'Губерниев', 'Физкультура');
INSERT INTO teachers(first_name, last_name, subject) VALUES ('Мария', 'Петрова', 'Физика');
insert into students_teachers values (1,1);
insert into students_teachers values (2,3);
insert into students_teachers values (1,4);
insert into students_teachers values (4,3);
insert into students_teachers values (3,2);

