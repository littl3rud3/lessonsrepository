import java.io.*;

public class ForexExchange implements ForexService {
    public Double exchange(String from, String to, int amount) throws IOException {
        ForexService forexService = new ForexExchange();
        return forexService.findExchangeRateAndConvert(from, to) * amount;
    }

}