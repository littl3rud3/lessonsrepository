import java.util.*;

public class HumanServiceImpl implements HumanService{
    @Override
    public List<Human> under20(List<Human> humans) {
        List<Human> list = new ArrayList<>();
        for (Human h : humans) {
            if (h.age <= 20 && h.secondName.matches("[абвгдАБВГД].*")) list.add(h);
            ;
        }
        return list;
    }
    @Override
    public boolean collectionContainsIt(Human human, List<Human> humans) {
        for (Human h : humans) {
            if (h.equals(human)) return true;
//            if(Objects.hash(h) == Objects.hash(humanity)) System.out.println("TRUE");
        }
        return false;
    }

    @Override
    public void dublicates(List<Human> humans) {
        SortedMap<Human, Integer> dubs = new TreeMap<>(new Comparator<Human>() {
            @Override
            public int compare(Human human, Human t1) {
                return human.age > t1.age ? human.age - t1.age : t1.age - human.age;
            }
        });
        int count = 0;
        for (Human h : humans) {
            int coun = (int) humans.stream().filter(x -> h.equals(x)).count();
            if (coun > 1) {
                count = coun;
                dubs.put(h, count);
            }
        }
        dubs.entrySet().stream().forEach(x-> System.out.println("{" +x.getValue() + "[{" + x.getKey().toString() + "}}]"));
    }
}
