import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.AssertJUnit.assertEquals;


public class ForexTest {
    @Test
    public void exchangeMoney() throws IOException {
        ForexExchange exchange = new ForexExchange();

        assertEquals(Double.valueOf(73.8605), exchange.exchange("USD", "RUB", 1));
        System.out.println(exchange.exchange("USD","RUB", 1));
    }
}
