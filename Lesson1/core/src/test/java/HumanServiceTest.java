import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class HumanServiceTest {

    Human human1 = new Human("Дмитрий", "Иванов", "Владимирович", 40, "М", LocalDate.of(1970, 11, 10));
    Human human2 = new Human("Кирилл", "Алексеев", "Алексеевич", 19, "М", LocalDate.of(1970, 11, 10));
    Human human3 = new Human("Александр", "Баранов", "Кириллович", 18, "М", LocalDate.of(1970, 11, 10));
    Human human4 = new Human("Александр", "Баранов", "Кириллович", 18, "М", LocalDate.of(1970, 11, 10));
    Human human5 = new Human("Дмитрий", "Иванов", "Владимирович", 40, "М", LocalDate.of(1970, 11, 10));

    List<Human> humans = new ArrayList<Human>(Arrays.asList(human1, human2, human3, human4, human5));

    @Test
    public void containsTest() {

        HumanServiceImpl ex = new HumanServiceImpl();

        Human humanity = new Human("Дмитрий", "Клименко", "Кириллович", 40, "М", LocalDate.of(1970, 11, 10));

        assertEquals(false, ex.collectionContainsIt(humanity, humans));

        ex.dublicates(humans);

    }
    @Test
    public void under20Test() {
        HumanServiceImpl ex = new HumanServiceImpl();
        assertEquals(Arrays.asList(human2, human3, human4), ex.under20(humans));
    }

}

