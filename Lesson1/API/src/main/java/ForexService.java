import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

public interface ForexService {

    default Double findExchangeRateAndConvert(String from, String to) throws IOException {
        StringBuilder sb = new StringBuilder();

        URL oracle = null;
        try {
            oracle = new URL("https://v6.exchangerate-api.com/v6/3a719f0e12151529ea9de8e2/latest/" + from.toUpperCase());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        URLConnection yc = null;
        try {
            yc = oracle.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(
                yc.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null)
            sb.append(inputLine.split("\"conversion_rates\":")[1].replaceAll("\"","").replaceAll("}}",""));
        in.close();

        String str = String.valueOf(sb);
        Map<String, Double> currency = new HashMap<>();
        for (String s : str.split(",")) {
            currency.put(s.split(":")[0], Double.parseDouble(s.split(":")[1]));
        }
        return currency.get(to.toUpperCase());
    }
}


