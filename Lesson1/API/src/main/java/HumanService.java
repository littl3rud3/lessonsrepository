import java.util.List;

public interface HumanService {
    List<Human> under20(List<Human> humans);

    boolean collectionContainsIt(Human human, List<Human> humans);

    void dublicates(List<Human> humans);
}
