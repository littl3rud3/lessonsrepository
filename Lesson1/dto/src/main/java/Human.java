import java.time.LocalDate;
import java.util.Objects;

public class Human {
    public String name;
    public String secondName;
    public String thirdName;
    int age;
    String sex;
    LocalDate birthDate;

    public Human(String name, String secondName, String thirdName, int age, String sex, LocalDate birthDate) {
        this.name = name;
        this.secondName = secondName;
        this.thirdName = thirdName;
        this.age = age;
        this.sex = sex;
        this.birthDate = birthDate;
    }


    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (name == null ? 0 : name.hashCode());
        result = 31 * result + (secondName == null ? 0 : secondName.hashCode());
        result = 31 * result + (thirdName == null ? 0 : thirdName.hashCode());
        result = 31 * result + (sex == null ? 0 : sex.hashCode());
        result = 31 * result + (birthDate == null ? 0 : birthDate.hashCode());
        result = 31 * result + age;
        return result;
    }

    @Override
    public boolean equals(Object other) {
        /* Check this and other refer to the same object */
        if (this == other) {
            return true;
        }

        /* Check other is Person and not null */
        if (!(other instanceof Human)) {
            return false;
        }

        Human person = (Human) other;

        /* Compare all required fields */
        return age == person.age &&
                Objects.equals(name, person.name) &&
                Objects.equals(secondName, person.secondName) &&
                Objects.equals(thirdName, person.thirdName) &&
                Objects.equals(sex, person.sex) &&
                Objects.equals(birthDate, person.birthDate);
    }

    @Override
    public String toString() {
        return name + " " + secondName + " " + thirdName + " " + age + " " + sex + " " + birthDate;
    }
}
