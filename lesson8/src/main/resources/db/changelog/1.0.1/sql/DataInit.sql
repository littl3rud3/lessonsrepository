CREATE TABLE jobs(
                     job_id varchar(10) PRIMARY KEY,
                     job_title varchar(35) NOT NULL,
                     min_salary int,
                     max_salary int,
                     CHECK ( max_salary > jobs.min_salary )

);
CREATE TABLE locations(
                          location_id int PRIMARY KEY NOT NULL,
                          street_address varchar(40),
                          city varchar(30) NOT NULL
);
CREATE TABLE departments(
                            department_id int PRIMARY KEY NOT NULL,
                            department_name varchar(30) NOT NULL,
                            manager_id int,
                            location_id int,
                            FOREIGN KEY (location_id) REFERENCES locations,
                            CHECK ( length(department_name) > 4 )
);
CREATE TABLE employees(
                          employee_id INT PRIMARY KEY NOT NULL,
                          first_name varchar(20),
                          last_name varchar(25) NOT NULL,
                          email varchar(25) UNIQUE NOT NULL,
                          phone_number varchar(20),
                          hire_date date NOT NULL,
                          job_id varchar(10) NOT NULL,
                          salary int,
                          manager_id integer,
                          department_id int,
                          FOREIGN KEY (manager_id) REFERENCES employees,
                          FOREIGN KEY (department_id) REFERENCES departments,
                          FOREIGN KEY (job_id) REFERENCES jobs,
                          CHECK ( length(first_name) >=2 ),
                          CHECK ( length(last_name) >=2 ),
                          CHECK ( length(email) > 5),
                          CHECK ( length(phone_number) > 10 )
);




CREATE TABLE job_history(
                            employee_id int NOT NULL,
                            start_date date NOT NULL,
                            end_date date,
                            job_id varchar(10) NOT NULL,
                            department_id int,
                            FOREIGN KEY (job_id) REFERENCES jobs(job_id),
                            FOREIGN KEY (department_id) REFERENCES departments(department_id),
                            FOREIGN KEY (employee_id) REFERENCES employees(employee_id),
                            CONSTRAINT employee_id PRIMARY KEY (employee_id, start_date),
                            CHECK ( start_date < end_date )

);






