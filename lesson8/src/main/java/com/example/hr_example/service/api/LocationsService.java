package com.example.hr_example.service.api;

import com.example.hr_example.DTO.LocationsDTO;

import java.util.List;

/**
 * Сервис для работы с лоакациями
 */
public interface LocationsService {

    /**
     * Получает сущность локации по ид
     *
     * @param id идентификатор
     * @return сохраненная сущность с ид.
     */
    LocationsDTO getLocationById(int id);

    /**
     * Вставляет новую сущность в базу данных
     *
     * @param locationsDTO сущность локации
     * @return сохраненная сущность
     */
    LocationsDTO insertLocation(LocationsDTO locationsDTO);

    /**
     * Получает все сущности локаций
     *
     * @return Все локации
     */
    List<LocationsDTO> getAll();
}
