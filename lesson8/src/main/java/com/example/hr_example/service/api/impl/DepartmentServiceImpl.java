package com.example.hr_example.service.api.impl;

import com.example.hr_example.DAO.DepartmentsMapper;
import com.example.hr_example.DTO.DepartmentsDTO;
import com.example.hr_example.entity.DepartmentsEntity;
import com.example.hr_example.service.api.DepartmentsService;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
//@Slf4j
@Log4j2
public class DepartmentServiceImpl implements DepartmentsService {

    @Autowired
    DepartmentsMapper departmentsMapper;

//    @Autowired
//    Logger logger;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public DepartmentsDTO getDepartmentById(int id) {
        log.error("Отображение пользователя с id={}", id);
        return modelMapper.map(departmentsMapper.selectByPrimaryKey(id), DepartmentsDTO.class);
    }

    @Override
    @Transactional
    public DepartmentsDTO insertDepartment(DepartmentsDTO departmentsDTO) {
        departmentsDTO.setLocationId(departmentsDTO.getLocation().getLocationId());
        departmentsMapper.insert(modelMapper.map(departmentsDTO, DepartmentsEntity.class));
        log.error("Добавление департамента с id={}", departmentsDTO.getDepartmentId());
        return departmentsDTO;
    }

    @Override
    public List<DepartmentsDTO> getAllDepartments() {
        List<DepartmentsEntity> departmentsEntities = departmentsMapper.allDepartments();
        log.error("Отображение всех департаментов");
        return modelMapper.map(departmentsEntities, new TypeToken<List<DepartmentsDTO>>() {}.getType());
    }

    @Override
    public void updateDepartment(DepartmentsDTO departmentsDTO) {
        log.error("Обновление данных в департаменте id={}", departmentsDTO.getDepartmentId());
        departmentsMapper.updateByPrimaryKey(modelMapper.map(departmentsDTO, DepartmentsEntity.class));
    }


    @Override
    public List<DepartmentsDTO> getAllByName(String name) {
        log.error("Отображение всех департаментов c конретным именем");
        return modelMapper.map(departmentsMapper.getByName(name), new TypeToken<List<DepartmentsDTO>>() {}.getType());
    }

    @Override
    public List<DepartmentsDTO> getAllBySurname(String surname) {
        log.error("Отображение всех департаментов с конкретной фамилией главы департамента");
        return modelMapper.map(departmentsMapper.getBySurname(surname), new TypeToken<List<DepartmentsDTO>>() {}.getType());
    }
}
