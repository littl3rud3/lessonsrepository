package com.example.hr_example.service.api;

import com.example.hr_example.DTO.DepartmentsDTO;
import com.example.hr_example.DTO.EmployeesDTO;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;


/**
 * Сервис для работы с работниками
 */
public interface EmployeesService {

    /**
     * Получает сущность работика по ид
     *
     * @param id идентификатор
     * @return сохраненная сущность с ид.
     */
    EmployeesDTO getEmployeeById(int id);

    /**
     * Сохраняет переданную сущность в базе данных
     *
     * @param employeesDTO сущность работника
     * @return сохраненная сущность
     */
    EmployeesDTO insertEmployee(EmployeesDTO employeesDTO);

    /**
     * Обновляет переданную сущность в базе данных
     *
     * @param employeesDTO сущность работника
     * @return обновленная сущность
     */
    void updateEmployee(EmployeesDTO employeesDTO);

    /**
     * Получает сущности работников
     *
     * @return Все сущности
     */
    List<EmployeesDTO> allEmployees();

    /**
     * Получает сущности работников
     *
     * @param name имя работника
     * @return Все сущности, отфильтрованные по имени работника
     */
    List<EmployeesDTO> getAllByName(String name);

    /**
     * Получает сущности работников
     *
     * @param surname фамилия работника
     * @return Все сущности, отфильтрованные по имени работника
     */
    List<EmployeesDTO> getAllBySurname(String surname);

    /**
     * Получает сущности работников
     *
     * @param email электронная почта работника
     * @return Все сущности, отфильтрованные по электронной почте
     */
    List<EmployeesDTO> getAllByEmail(String email);

    /**
     * Получает сущности работников
     *
     * @param start начало интервала
     * @param end конец интервала
     * @return Все сущности, отфильтрованные по дате найма на работу
     */
    List<EmployeesDTO> getAllByHireDate(Date start, Date end);
}
