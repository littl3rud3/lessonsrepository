package com.example.hr_example.service.api.impl;

import com.example.hr_example.DAO.DepartmentsMapper;
import com.example.hr_example.DAO.EmployeesMapper;
import com.example.hr_example.DAO.JobsMapper;
import com.example.hr_example.DTO.EmployeesDTO;
import com.example.hr_example.entity.DepartmentsEntity;
import com.example.hr_example.entity.EmployeesEntity;
import com.example.hr_example.entity.JobsEntity;
import com.example.hr_example.service.api.EmployeesService;
import com.example.hr_example.entity.JobHistoryEntity;
import com.example.hr_example.DAO.JobHistoryMapper;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Log4j2
public class EmployeesServiceImpl implements EmployeesService {

    @Autowired
    private EmployeesMapper employeesMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    JobHistoryMapper jobHistoryMapper;

    @Autowired
    JobsMapper jobsMapper;

    @Autowired
    DepartmentsMapper departmentsMapper;

    @Override
    public EmployeesDTO getEmployeeById(int id) {
        EmployeesDTO employeesDTO = modelMapper.map(employeesMapper.allInfo(id), EmployeesDTO.class);
        return employeesDTO;
    }

    @Override
    @Transactional
    public EmployeesDTO insertEmployee(EmployeesDTO employeesDTO) {
        if (employeesDTO.getJobInfo() != null) {
            if (jobsMapper.selectByPrimaryKey(employeesDTO.getJobInfo().getJobId()) == null) {
                if (employeesDTO.getJobInfo().getJobTitle() != null && employeesDTO.getJobInfo().getMinSalary() != null
                && employeesDTO.getJobInfo().getMaxSalary() != null) {
                    JobsEntity jobsEntity = new JobsEntity();
                    jobsEntity.setJobId(employeesDTO.getJobInfo().getJobId());
                    jobsEntity.setJobTitle(employeesDTO.getJobInfo().getJobTitle());
                    jobsEntity.setMinSalary(employeesDTO.getJobInfo().getMinSalary());
                    jobsEntity.setMaxSalary(employeesDTO.getJobInfo().getMaxSalary());
                    jobsMapper.insert(jobsEntity);
                } else throw new RuntimeException("No such job found");
            }
        }
        if (employeesDTO.getDepartment() != null) {
            if (departmentsMapper.selectByPrimaryKey(employeesDTO.getDepartmentId()) == null) {
                DepartmentsEntity departmentsEntity = new DepartmentsEntity();
                departmentsEntity.setDepartmentId(employeesDTO.getDepartmentId());
                departmentsEntity.setDepartmentName(employeesDTO.getDepartment().getDepartmentName());
                departmentsEntity.setLocationId(employeesDTO.getDepartment().getLocationId());
                departmentsMapper.insert(departmentsEntity);
            }
        }
        employeesMapper.insert(modelMapper.map(employeesDTO, EmployeesEntity.class));
        JobHistoryEntity jobHistoryEntity = new JobHistoryEntity();
        jobHistoryEntity.setJobId(employeesDTO.getJobInfo().getJobId());
        jobHistoryEntity.setEmployeeId(employeesDTO.getEmployeeId());
        jobHistoryEntity.setDepartmentId(employeesDTO.getDepartment().getDepartmentId());
        jobHistoryEntity.setStartDate(employeesDTO.getHireDate());
        jobHistoryMapper.insert(jobHistoryEntity);

        log.error("Добавлен новый работник");
        return employeesDTO;
    }


    @Override
    public void updateEmployee(EmployeesDTO employeesDTO) {
        if (employeesDTO.getJobInfo() != null) {
            JobHistoryEntity jobHistoryEntity = new JobHistoryEntity();
            jobHistoryEntity.setJobId(employeesDTO.getJobInfo().getJobId());
            jobHistoryEntity.setEmployeeId(employeesDTO.getEmployeeId());
            jobHistoryEntity.setDepartmentId(employeesDTO.getDepartment().getDepartmentId());
            jobHistoryEntity.setStartDate(employeesDTO.getHireDate());
            jobHistoryMapper.insert(jobHistoryEntity);
        }
        employeesMapper.updateByPrimaryKey(modelMapper.map(employeesDTO, EmployeesEntity.class));

        log.error("Обновлены данные работника с id={}", employeesDTO.getEmployeeId());
//        JobHistoryEntity jobHistoryEntity = new JobHistoryEntity();
//        jobHistoryEntity.setDepartmentId(employeesDTO.getDepartmentId());
//        jobHistoryEntity.setJobId(employeesDTO.getJobId());
//        jobHistoryEntity.setEmployeeId(employeesDTO.getEmployeeId());
//        jobHistoryEntity.setStartDate(employeesDTO.getHireDate());
//        jobHistoryMapper.updateByPrimaryKey(jobHistoryEntity);
    }

    @Override
    public List<EmployeesDTO> allEmployees() {
        log.error("Все работники отображены");
        return modelMapper.map(employeesMapper.allEmployees(), new TypeToken<List<EmployeesDTO>>() {}.getType());
    }

    @Override
    public List<EmployeesDTO> getAllByName(String name) {
        log.error("Все работники с конкретным именем отображены");
        return modelMapper.map(employeesMapper.getAllByName(name), new TypeToken<List<EmployeesDTO>>() {}.getType());
    }

    @Override
    public List<EmployeesDTO> getAllBySurname(String surname) {
        log.error("Все работники с конкретной фамилией отражены");
        return modelMapper.map(employeesMapper.getAllBySurname(surname), new TypeToken<List<EmployeesDTO>>() {}.getType());
    }

    @Override
    public List<EmployeesDTO> getAllByEmail(String email) {
        log.debug("Все работники с конкретной почтой");
        return modelMapper.map(employeesMapper.getAllByEmail(email), new TypeToken<List<EmployeesDTO>>() {}.getType());
    }

    @Override
    public List<EmployeesDTO> getAllByHireDate(Date start, Date end) {
        log.error("Все работники, принятые на работу в определенный промежуток времени");
        return modelMapper.map(employeesMapper.getAllByHireDate(start, end), new TypeToken<List<EmployeesDTO>>() {}.getType());
    }
}

