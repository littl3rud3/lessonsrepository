package com.example.hr_example.service.api.impl;

import com.example.hr_example.DAO.LocationsMapper;
import com.example.hr_example.DTO.LocationsDTO;
import com.example.hr_example.entity.LocationsEntity;
import com.example.hr_example.service.api.LocationsService;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.beans.Transient;
import java.util.List;

@Service
@Log4j2
public class LocationsServiceImpl implements LocationsService {

    @Autowired
    private LocationsMapper locationsMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public LocationsDTO getLocationById(int id) {
        LocationsEntity locationsEntity = locationsMapper.getLocationById(id).orElseThrow(() -> new RuntimeException("No such location"));
        log.error("Получение сущности с id {}", id);
        return modelMapper.map(locationsEntity, LocationsDTO.class);
    }

    @Override
    @Transactional
    public LocationsDTO insertLocation(LocationsDTO locationsDTO) {
        LocationsEntity locationsEntity = modelMapper.map(locationsDTO, LocationsEntity.class);
        locationsMapper.insert(locationsEntity);
        log.error("Создание новой локации {}", locationsDTO.getLocationId());
        return getLocationById(locationsEntity.getLocationId());
    }

    @Override
    @Transactional(readOnly = true)
    public List<LocationsDTO> getAll() {
        List<LocationsEntity> locationsEntities = locationsMapper.getAll();
        log.error("Все доступные локации");
        return modelMapper.map(locationsEntities, TypeToken.of(List.class).getType());
    }

}
