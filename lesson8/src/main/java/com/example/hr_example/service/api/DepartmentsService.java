package com.example.hr_example.service.api;

import com.example.hr_example.DTO.DepartmentsDTO;
import com.example.hr_example.DTO.LocationsDTO;
import com.example.hr_example.entity.DepartmentsEntity;

import java.util.List;


/**
 * Сервис для работы с департаментами
 */
public interface DepartmentsService {

    /**
     * Получает сущность департамента по ид
     *
     * @param id идентификатор
     * @return сохраненная сущность с ид.
     */
    DepartmentsDTO getDepartmentById(int id);


    /**
     * Сохраняет переданную сущность в базе данных
     *
     * @param departmentsDTO сущность департамента
     * @return сохраненная сущность
     */
    DepartmentsDTO insertDepartment(DepartmentsDTO departmentsDTO);

    /**
     * Получает сущности локаций
     *
     * @return Все сущности
     */
    List<DepartmentsDTO> getAllDepartments();

    /**
     * Обновляет переданную сущность в базе данных
     *
     * @param departmentsDTO сущность департамента
     * @return обновленная сущность
     */
    void updateDepartment(DepartmentsDTO departmentsDTO);

    /**
     * Получает сущности локаций
     *
     * @param name название департамента
     * @return Все сущности, отфильтрованные по названию департамента
     */
    List<DepartmentsDTO> getAllByName(String name);

    /**
     * Получает сущности локаций
     *
     * @param surname Фамилия главы департамента
     * @return Все сущности, отфильтрованные по фамилии главы департамента
     */
    List<DepartmentsDTO> getAllBySurname(String surname);
}
