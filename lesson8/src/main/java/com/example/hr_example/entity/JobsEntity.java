package com.example.hr_example.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * jobs
 * @author 
 */
@Data
public class JobsEntity implements Serializable {
    private String jobId;

    private String jobTitle;

    private Integer minSalary;

    private Integer maxSalary;

    private static final long serialVersionUID = 1L;
}