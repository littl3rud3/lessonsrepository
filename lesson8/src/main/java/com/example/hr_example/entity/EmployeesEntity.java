package com.example.hr_example.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * employees
 * @author 
 */
@Data
@Component
public class EmployeesEntity implements Serializable {
    private Integer employeeId;

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private Date hireDate;

    private String jobId;

    private Integer salary;

    private Integer managerId;

    private Integer departmentId;

    private JobsEntity jobInfo;

    private DepartmentsEntity department;

    private static final long serialVersionUID = 1L;
}