package com.example.hr_example.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * job_history
 * @author 
 */
@Data
public class JobHistoryEntity implements Serializable {

    private Integer employeeId;

    private Date startDate;

    private Date endDate;

    private String jobId;

    private Integer departmentId;

    private static final long serialVersionUID = 1L;
}