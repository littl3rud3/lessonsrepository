package com.example.hr_example.entity;

import java.io.Serializable;
import java.util.List;

import com.example.hr_example.DTO.EmployeesDTO;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * departments
 * @author 
 */
@Data
@Component
public class DepartmentsEntity implements Serializable {

    private Integer departmentId;

    private String departmentName;

    private Integer managerId;

    private Integer locationId;

    private LocationsEntity location;

    private EmployeesEntity manager;

    private static final long serialVersionUID = 1L;
}