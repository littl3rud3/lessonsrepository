package com.example.hr_example.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class LocationsEntity {
    private Integer locationId;

    private String streetAddress;

    private String city;
}
