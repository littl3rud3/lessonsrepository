package com.example.hr_example.DTO;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;


/**
 * Модель локации
 */
@Data
@Schema(title = "Модель данных локации")
public class LocationsDTO {
    @Schema(title = "Идентификатор локации")
    private Integer locationId;

    @Schema(title = "Адрес")
    private String streetAddress;

    @Schema(title = "Город")
    private String city;
}
