package com.example.hr_example.DTO;

import com.example.hr_example.entity.DepartmentsEntity;
import com.example.hr_example.entity.JobsEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Schema(title = "Модель данных работника")
public class EmployeesDTO {

    @Schema(title = "Уникальный идентификатор работника")
    private Integer employeeId;

    @Schema(title = "Имя работника")
    private String firstName;

    @Schema(title = "Фамилия работника")
    private String lastName;

    @Schema(title = "Электронная почта работника")
    private String email;

    @Schema(title = "Номер телефона работнка")
    private String phoneNumber;

    @Schema(title = "Дата приема на работу")
    private Date hireDate;

    @Schema(title = "Уникальный идентификатор работы работника")
    private String jobId;

    @Schema(title = "Текущая зарплпта работника")
    private Integer salary;

    @Schema(title = "Уникальный идентификатор менеджера работника")
    private Integer managerId;

    @Schema(title = "Уникальный идентификатор департамента, где работник")
    private Integer departmentId;

    @Schema(title = "Подробная информация о работе")
    private JobsEntity jobInfo;

    @Schema(title = "Подбробная информация о департаменте")
    private DepartmentsEntity department;

}
