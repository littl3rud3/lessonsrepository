package com.example.hr_example.DTO;

import com.example.hr_example.entity.EmployeesEntity;
import lombok.Data;

@Data
public class DepartmentsDTO {
    private Integer departmentId;

    private String departmentName;

    private Integer managerId;

    private Integer locationId;

    private LocationsDTO location;

    private EmployeesDTO manager;
}
