package com.example.hr_example.DAO;


import com.example.hr_example.entity.LocationsEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface LocationsMapper {

    @Select("SELECT * FROM locations WHERE location_id = #{id}")
    Optional<LocationsEntity> getLocationById(int id);

    @Insert("INSERT INTO locations(street_address, city) VALUES (#{streetAddress}, #{city})")
//    @Options(keyProperty = )
    void insert(LocationsEntity locationsEntity);

    @Select("SELECT * FROM locations")
    List<LocationsEntity> getAll();

}
