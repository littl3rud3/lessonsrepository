package com.example.hr_example.DAO;

import com.example.hr_example.entity.DepartmentsEntity;
import com.example.hr_example.entity.LocationsEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DepartmentsMapper {

    int deleteByPrimaryKey(Integer departmentId);

    int insert(DepartmentsEntity record);

    int insertSelective(DepartmentsEntity record);

    DepartmentsEntity selectByPrimaryKey(Integer departmentId);

    int updateByPrimaryKeySelective(DepartmentsEntity record);

    int updateByPrimaryKey(DepartmentsEntity record);

    List<DepartmentsEntity> allDepartments();

    DepartmentsEntity getAllJoins(int id);

    List<DepartmentsEntity> getByName(String name);

    List<DepartmentsEntity> getBySurname(String surname);
}