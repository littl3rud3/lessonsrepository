package com.example.hr_example.DAO;

import com.example.hr_example.entity.JobsEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface JobsMapper {
    int deleteByPrimaryKey(String jobId);

    int insert(JobsEntity record);

    int insertSelective(JobsEntity record);

    JobsEntity selectByPrimaryKey(String jobId);

    int updateByPrimaryKeySelective(JobsEntity record);

    int updateByPrimaryKey(JobsEntity record);
}