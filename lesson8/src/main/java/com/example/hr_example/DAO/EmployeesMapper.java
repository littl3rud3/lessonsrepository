package com.example.hr_example.DAO;

import com.example.hr_example.entity.EmployeesEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;


@Mapper
@Repository
public interface EmployeesMapper {
    int deleteByPrimaryKey(Integer employeeId);

    int insert(EmployeesEntity record);

    int insertSelective(EmployeesEntity record);

    EmployeesEntity selectByPrimaryKey(Integer employeeId);

    int updateByPrimaryKeySelective(EmployeesEntity record);

    int updateByPrimaryKey(EmployeesEntity record);

    EmployeesEntity allInfo(int id);

    List<EmployeesEntity> allEmployees();

    List<EmployeesEntity> getAllBySurname(String surname);

    List<EmployeesEntity> getAllByName(String name);

    List<EmployeesEntity> getAllByEmail(String email);

    List<EmployeesEntity> getAllByHireDate(Date start, Date end);
}