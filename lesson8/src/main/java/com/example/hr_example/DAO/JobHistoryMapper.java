package com.example.hr_example.DAO;

import com.example.hr_example.entity.JobHistoryEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface JobHistoryMapper {

    int insert(JobHistoryEntity record);

    int insertSelective(JobHistoryEntity record);

    int updateByPrimaryKeySelective(JobHistoryEntity record);

    int updateByPrimaryKey(JobHistoryEntity record);
}