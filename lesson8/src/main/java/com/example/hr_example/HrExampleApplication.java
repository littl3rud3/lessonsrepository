package com.example.hr_example;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

@SpringBootApplication
public class HrExampleApplication {

    @Autowired
    private DataSource dataSource;


    public static void main(String[] args) {

        SpringApplication.run(HrExampleApplication.class, args);
    }

    @Bean
    public ModelMapper ModelMapperBean() {
        return new ModelMapper();
    }
}
