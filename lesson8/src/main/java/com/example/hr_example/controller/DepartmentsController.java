package com.example.hr_example.controller;

import com.example.hr_example.DTO.DepartmentsDTO;
import com.example.hr_example.DTO.LocationsDTO;
import com.example.hr_example.service.api.DepartmentsService;
import com.example.hr_example.service.api.LocationsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/departments")
@RequiredArgsConstructor
@Tag(name = "department", description = "АПИ для департаментов")
public class DepartmentsController {

    @Autowired
    DepartmentsService departmentsService;

    @GetMapping()
    @Operation(summary = "Все доступные департаменты")
    public List<DepartmentsDTO> allDepartments(@RequestParam(required = false) String name, @RequestParam(required = false) String surname) {
        if (name != null) {
            return departmentsService.getAllByName(name);
        } else if (surname != null) {
            return departmentsService.getAllBySurname(surname);
        }
        return departmentsService.getAllDepartments();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Найти департамент по идентификатору")
    public DepartmentsDTO getDepartmentById(@PathVariable int id) {
        return departmentsService.getDepartmentById(id);
    }

    @PostMapping("/add")
    @Operation(summary = "Добавить департамент")
    public DepartmentsDTO insertLocation(@RequestBody DepartmentsDTO departmentsDTO) {
        return departmentsService.insertDepartment(departmentsDTO);
    }

    @PutMapping("/edit/{id}")
    @Operation(summary = "Изменить информацию о департаментк")
    public DepartmentsDTO updateDepartment(@PathVariable int id, @RequestBody DepartmentsDTO departmentsDTO) {
        if (departmentsDTO.getDepartmentId() != id) throw new RuntimeException("Access denied");
        departmentsService.updateDepartment(departmentsDTO);
        return departmentsService.getDepartmentById(departmentsDTO.getDepartmentId());
    }

//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public List<DepartmentsDTO> filterByName(@RequestParam String name) {
//        return departmentsService.getAllByName(name);
//    }
//
//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public List<DepartmentsDTO> filterBySurname(@RequestParam String surname) {
//        return departmentsService.getAllBySurname(surname);
//    }
}
