package com.example.hr_example.controller;

import com.example.hr_example.DTO.LocationsDTO;
import com.example.hr_example.service.api.LocationsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/locations")
@RequiredArgsConstructor
@Tag(name = "location", description = "АПИ для локаций")
public class LocationsController {

    @Autowired
    LocationsService locationsService;

    @GetMapping()
    @Operation(summary = "Все доступные локации")
    public List<LocationsDTO> allLocations() {
        return locationsService.getAll();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Найти локацию по идентификатору")
    public LocationsDTO getLocationById(@PathVariable int id) {
        return locationsService.getLocationById(id);
    }

    @PostMapping("/add")
    @Operation(summary = "Добавить локацию")
    public LocationsDTO insertLocation(@RequestBody LocationsDTO locationsDTO) {
        return locationsService.insertLocation(locationsDTO);
    }
}
