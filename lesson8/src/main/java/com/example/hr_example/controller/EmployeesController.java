package com.example.hr_example.controller;


import com.example.hr_example.DTO.EmployeesDTO;
import com.example.hr_example.service.api.EmployeesService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/employees")
@RequiredArgsConstructor
@Tag(name = "employees", description = "АПИ для работников")
public class EmployeesController {

    @Autowired
    private EmployeesService employeesService;

    @GetMapping
    @Operation(summary = "Все работники")
    public List<EmployeesDTO> allEmployees(@RequestParam(required = false) String surname, @RequestParam(required = false) String name,
                                           @RequestParam(required = false) String email, @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
                                           @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date end) {
        if (surname != null) {
            return employeesService.getAllBySurname(surname);
        } else if (name != null) {
            return employeesService.getAllByName(name);
        } else if (email != null) {
            return employeesService.getAllByEmail(email);
        } else if (start != null && end != null) {
            return employeesService.getAllByHireDate(start, end);
        }
        return employeesService.allEmployees();

    }

    @GetMapping("/{id}")
    @Operation(summary = "Получение конкретного работника по идентификатору")
    public EmployeesDTO getById(@PathVariable Integer id) {
        return employeesService.getEmployeeById(id);
    }

    @PostMapping("/add")
    @Operation(summary = "Добавление нового работника")
    public EmployeesDTO addNew(@RequestBody EmployeesDTO employeesDTO) {
        employeesDTO.setJobId(employeesDTO.getJobInfo().getJobId());
        employeesDTO.setDepartmentId(employeesDTO.getDepartment().getDepartmentId());
        employeesService.insertEmployee(employeesDTO);
        return employeesDTO;
    }

    @PutMapping("/edit/{id}")
    @Operation(summary = "Изменить данные работника")
    public EmployeesDTO edit(@PathVariable int id, @RequestBody EmployeesDTO employeesDTO) {
        if (employeesDTO.getEmployeeId() != id) throw new RuntimeException("Access denied");
        employeesService.updateEmployee(employeesDTO);
        return employeesService.getEmployeeById(employeesDTO.getEmployeeId());
    }
}
