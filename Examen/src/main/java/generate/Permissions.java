package generate;

import java.io.Serializable;
import lombok.Data;

/**
 * permissions
 * @author 
 */
@Data
public class Permissions extends PermissionsKey implements Serializable {
    private String permission;

    private static final long serialVersionUID = 1L;
}