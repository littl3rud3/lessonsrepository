package generate;

import java.io.Serializable;
import lombok.Data;

/**
 * students_teachers
 * @author 
 */
@Data
public class StudentsTeachersEntityKey implements Serializable {
    private Integer idStudent;

    private Integer idTeacher;

    private static final long serialVersionUID = 1L;
}