package generate;

import generate.Permissions;
import generate.PermissionsKey;

public interface PermissionsDao {
    int deleteByPrimaryKey(PermissionsKey key);

    int insert(Permissions record);

    int insertSelective(Permissions record);

    Permissions selectByPrimaryKey(PermissionsKey key);

    int updateByPrimaryKeySelective(Permissions record);

    int updateByPrimaryKey(Permissions record);
}