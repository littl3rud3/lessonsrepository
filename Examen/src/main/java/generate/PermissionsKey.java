package generate;

import java.io.Serializable;
import lombok.Data;

/**
 * permissions
 * @author 
 */
@Data
public class PermissionsKey implements Serializable {
    private Integer permissionId;

    private Integer roleId;

    private static final long serialVersionUID = 1L;
}