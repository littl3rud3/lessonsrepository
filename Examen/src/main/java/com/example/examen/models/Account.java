package com.example.examen.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    private Long userId;

    private String username;

    private String password;

    private String country;

    private int roleId;

}