package com.example.examen.models;


import lombok.Data;

@Data
public class studentTeachersEntity {

    private Integer idStudent;

    private Integer idTeacher;

}
