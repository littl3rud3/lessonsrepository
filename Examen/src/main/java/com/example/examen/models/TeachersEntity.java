package com.example.examen.models;

import java.io.Serializable;
import lombok.Data;

/**
 * teachers
 * @author 
 */
@Data
public class TeachersEntity implements Serializable {
    private Integer id;

    private String fullName;

    private String department;

    private static final long serialVersionUID = 1L;
}