package com.example.examen.models;

import java.io.Serializable;
import lombok.Data;

/**
 * students
 * @author 
 */
@Data
public class StudentsEntity implements Serializable {
    private Integer id;

    private String fullName;

    private String specialty;

    private String course;

    private static final long serialVersionUID = 1L;
}