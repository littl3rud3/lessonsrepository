package com.example.examen.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountEntity {
    private Long userId;

    private String username;

    private String password;

    private Long country;

    private int roleId;
}
