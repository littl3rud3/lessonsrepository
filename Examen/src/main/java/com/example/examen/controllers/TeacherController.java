package com.example.examen.controllers;

import com.example.examen.dto.StudentDTO;
import com.example.examen.dto.TeacherDTO;
import com.example.examen.dto.studentTeachersDTO;
import com.example.examen.services.TeacherService;
import com.example.examen.services.stService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/teachers")
public class TeacherController {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private stService stservice;

    @GetMapping()
    public List<TeacherDTO> showAll(){
        jmsTemplate.convertAndSend("Teacher", teacherService.allTeachers());
        return teacherService.allTeachers();
    }

    @PostMapping("/add")
    public void addTeacher(@RequestBody TeacherDTO teacherDTO){
        teacherService.addTeacher(teacherDTO);
        jmsTemplate.convertAndSend("Teacher", teacherDTO);
    }

    @DeleteMapping("/{id}/delete")
    public void removeTeacher(@PathVariable int id) {
        jmsTemplate.convertAndSend("Teacher", teacherService.findById(id));
        teacherService.deleteTeacher(id);
    }

    @PutMapping("{id}/edit")
    public void edit(@PathVariable int id, @RequestBody TeacherDTO teacherDTO) {
        teacherService.edit(teacherDTO);
        jmsTemplate.convertAndSend("Teacher", teacherService.findById(id));
    }

    @GetMapping("{id}")
    public TeacherDTO showOne(@PathVariable int id) {
        jmsTemplate.convertAndSend("Teacher", teacherService.findById(id));
        return teacherService.findById(id);
    }

//    @PostMapping("/{id}/add-student")
//    public void addLesson(@PathVariable int id, @RequestBody studentTeachersDTO studentTeachersDTO) {
//        studentTeachersDTO.setIdTeacher(id);
//        stservice.addNew(studentTeachersDTO.getIdStudent(),id);
//    }

    @DeleteMapping("/{id}/remove-students")
    public void removeLesson(@PathVariable int id, @RequestBody List<studentTeachersDTO> studentTeachersDTOs) {
        jmsTemplate.convertAndSend("StudentTeacher", studentTeachersDTOs);
        stservice.remove(studentTeachersDTOs);
    }

    @GetMapping("/{id}/all-students")
    public List<StudentDTO> allStudents(@PathVariable int id) {
        jmsTemplate.convertAndSend("StudentTeacher", stservice.allStudents(id));
        return stservice.allStudents(id);
    }

    @ExceptionHandler(NoSuchElementException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public HashMap<String, String> handleWrongId(Exception e) {
        HashMap<String, String> response = new HashMap<>();
        response.put("message", "There is no teacher with such id");
        response.put("error", e.getClass().getSimpleName());
        return response;
    }

//    @ExceptionHandler(org.postgresql.util.PSQLException.class)
//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
//    public HashMap<String, String> handleWrongParameter(Exception e) {
//        HashMap<String, String> response = new HashMap<>();
//        response.put("message", "There is something wrong in your parameters");
//        response.put("error", e.getClass().getSimpleName());
//        return response;
//    }
}

