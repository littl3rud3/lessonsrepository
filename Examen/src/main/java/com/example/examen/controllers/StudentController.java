package com.example.examen.controllers;

import com.example.examen.dto.StudentDTO;
import com.example.examen.dto.TeacherDTO;
import com.example.examen.dto.studentTeachersDTO;
import com.example.examen.models.studentTeachersEntity;
import com.example.examen.services.StudentService;
import com.example.examen.services.stService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private StudentService studentService;

    @Autowired
    private stService stservice;

    @GetMapping()
    public List<StudentDTO> showAll() {
        jmsTemplate.convertAndSend("Student", studentService.allStudents());
        return studentService.allStudents();
    }

    @PostMapping("/add")
    public void addStudent(@RequestBody StudentDTO studentDTO) {
        studentService.addStudent(studentDTO);
        jmsTemplate.convertAndSend("Student", studentDTO);
    }

    @DeleteMapping("/{id}/delete")
    public void removeStudent(@PathVariable int id) {
        jmsTemplate.convertAndSend("Student", studentService.findById(id));
        studentService.deleteStudent(id);
    }

    @GetMapping("{id}")
    public StudentDTO showOne(@PathVariable int id) throws Exception {
        jmsTemplate.convertAndSend("Student", studentService.findById(id));
        return studentService.findById(id);
    }

    @PutMapping("{id}/edit")
    public void edit(@PathVariable int id, @RequestBody StudentDTO studentDTO) {
        studentService.edit(studentDTO);
        jmsTemplate.convertAndSend("Student", studentService.findById(id));
    }

    @PostMapping("/{id}/add-lessons")
    public void addLesson(@PathVariable int id, @RequestBody List<studentTeachersDTO> studentTeachersDTOS) {
        jmsTemplate.convertAndSend("StudentTeacher", studentTeachersDTOS);
        stservice.addNew(studentTeachersDTOS);
    }

    @DeleteMapping("/{id}/remove-lessons")
    public void removeLesson(@PathVariable int id, @RequestBody List<studentTeachersDTO> studentTeachersDTOs) {
        jmsTemplate.convertAndSend("StudentTeacher", studentTeachersDTOs);
        stservice.remove(studentTeachersDTOs);
    }

    @GetMapping("/{id}/all-lessons")
    public List<TeacherDTO> allLessons(@PathVariable int id) {
        if (studentService.findById(id) != null) {
            jmsTemplate.convertAndSend("StudentTeacher", stservice.allTeachers(id));
            return stservice.allTeachers(id);
        }
        else throw new NoSuchElementException();
    }

    @ExceptionHandler(NoSuchElementException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public HashMap<String, String> handleWrongId(Exception e) {
        HashMap<String, String> response = new HashMap<>();
        response.put("message", "There is no student with such id");
        response.put("error", e.getClass().getSimpleName());
        return response;
    }

//    @ExceptionHandler(org.postgresql.util.PSQLException.class)
//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
//    public HashMap<String, String> handleWrongParameter(Exception e) {
//        HashMap<String, String> response = new HashMap<>();
//        response.put("message", "There is something wrong in your parameters");
//        response.put("error", e.getClass().getSimpleName());
//        return response;
//    }
}