package com.example.examen.controllers;

import com.example.examen.dto.LoginRequestDTO;
import com.example.examen.dto.RegistrationRequestDTO;
import com.example.examen.models.Account;
import com.example.examen.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/api")
public class AccountController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private JmsTemplate jmsTemplate;

    @PostMapping("/authorize")
    public Authentication authorize(@RequestBody @Validated LoginRequestDTO loginRequestDTO) {
        jmsTemplate.convertAndSend("Login", loginRequestDTO);
        return authenticationService.authorize(loginRequestDTO);
    }

    @PostMapping("/register")
    public Account register(@RequestBody @Validated RegistrationRequestDTO registrationRequestDTO) {
        jmsTemplate.convertAndSend("Registration", registrationRequestDTO);
        return authenticationService.register(registrationRequestDTO);
    }

    @PostMapping("/logout")
    public void logout() {
        authenticationService.logout();
    }

    @PostMapping("/current")
    public Principal current(Principal principal) {
        return principal;
    }
}
