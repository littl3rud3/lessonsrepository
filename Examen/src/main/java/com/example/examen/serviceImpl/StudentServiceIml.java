package com.example.examen.serviceImpl;

import com.example.examen.dao.StudentsMapper;
import com.example.examen.dto.StudentDTO;
import com.example.examen.models.StudentsEntity;
import com.example.examen.services.StudentService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceIml implements StudentService {

    @Autowired
    StudentsMapper studentsMapper;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public List<StudentDTO> allStudents() {
        List<StudentsEntity> allStudents= studentsMapper.allStudents();
        return modelMapper.map(allStudents, new TypeToken<List<StudentDTO>>() {}.getType());
    }

    @Override
    public StudentDTO findById(int id) {
        return modelMapper.map(studentsMapper.selectByPrimaryKey(id), StudentDTO.class);
    }

    @Override
    public void addStudent(StudentDTO studentDTO) {
        studentsMapper.insert(modelMapper.map(studentDTO,StudentsEntity.class));

    }

    @Override
    public void deleteStudent(int id) {
        studentsMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void edit(StudentDTO studentDTO) {
        studentsMapper.updateByPrimaryKey(modelMapper.map(studentDTO, StudentsEntity.class));
    }
}
