package com.example.examen.serviceImpl;

import com.example.examen.dao.TeachersMapper;
import com.example.examen.dto.StudentDTO;
import com.example.examen.dto.TeacherDTO;
import com.example.examen.models.StudentsEntity;
import com.example.examen.models.TeachersEntity;
import com.example.examen.services.TeacherService;
import com.example.examen.services.stService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    private TeachersMapper teachersMapper;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    private stService stServ;

    public List<TeacherDTO> allTeachers() {
        List<TeachersEntity> allTeachers= teachersMapper.allTeachers();
        return modelMapper.map(allTeachers, new TypeToken<List<TeacherDTO>>() {}.getType());
    }

    public TeacherDTO findById(int id) {
        return modelMapper.map(teachersMapper.selectByPrimaryKey(id), TeacherDTO.class);
    }

    public void addTeacher(TeacherDTO teacherDTO) {
        teachersMapper.insert(modelMapper.map(teacherDTO, TeachersEntity.class));
    }

    public void deleteTeacher(int id) {
        teachersMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void edit(TeacherDTO teacherDTO) {
        teachersMapper.updateByPrimaryKey(modelMapper.map(teacherDTO, TeachersEntity.class));
    }
}
