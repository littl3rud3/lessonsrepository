package com.example.examen.serviceImpl;

import com.example.examen.dao.StudentsTeachersMapper;
import com.example.examen.dto.StudentDTO;
import com.example.examen.dto.TeacherDTO;
import com.example.examen.dto.studentTeachersDTO;
import com.example.examen.models.studentTeachersEntity;
import com.example.examen.services.StudentService;
import com.example.examen.services.TeacherService;
import com.example.examen.services.stService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class stServiceImpl implements stService {

        @Autowired
        private StudentsTeachersMapper studentsTeachersMapper;

        @Autowired
        private TeacherService teacherService;

        @Autowired
        private StudentService studentService;

        @Autowired
        ModelMapper modelMapper;


        public void addNew(List<studentTeachersDTO> studentTeachersDTOS){
            studentsTeachersMapper.save(modelMapper.map(studentTeachersDTOS, new TypeToken<List<studentTeachersEntity>>() {}.getType()));
        }

        public void remove(List<studentTeachersDTO> studentTeachersDTOS) {
            studentsTeachersMapper.remove(modelMapper.map(studentTeachersDTOS, new TypeToken<List<studentTeachersEntity>>() {}.getType())) ;
        }

        public List<TeacherDTO> allTeachers(int id) {
            List<TeacherDTO> res = new ArrayList<>();
            List<Integer> v = studentsTeachersMapper.allStudentsId(id);
            for (Integer i : v) {
                res.add(teacherService.findById(i));
            }
            return res;
        }

        public List<StudentDTO> allStudents(int id) {
            List<StudentDTO> res = new ArrayList<>();
            List<Integer> v = studentsTeachersMapper.allTeachersId(id);
            for (Integer i : v) {
                res.add(studentService.findById(i));
            }
            return res;
        }
    }

