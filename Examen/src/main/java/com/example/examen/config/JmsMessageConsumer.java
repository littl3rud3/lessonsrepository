package com.example.examen.config;

import com.example.examen.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JmsMessageConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(JmsMessageConsumer.class);

    @JmsListener(destination = "Student")
    public void messageListener(StudentDTO studentDTO) {
        LOGGER.info("Message received! {}", studentDTO);
    }

    @JmsListener(destination = "Teacher")
    public void messageListener(TeacherDTO teacherDTO) {
        LOGGER.info("Message received! {}", teacherDTO);
    }

    @JmsListener(destination = "Student")
    public void messagesListener(List<StudentDTO> studentDTOS) {
        LOGGER.info("Message received! {}", studentDTOS);
    }

    @JmsListener(destination = "Teacher")
    public void messageListener(List<TeacherDTO> teacherDTOS) {
        LOGGER.info("Message received! {}", teacherDTOS);
    }

    @JmsListener(destination = "StudentTeacher")
    public void messagessListener(List<studentTeachersDTO> studentTeachersDTOs) {
        LOGGER.info("Message received! {}", studentTeachersDTOs);
    }

    @JmsListener(destination = "Registration")
    public void regListener(RegistrationRequestDTO registrationRequestDTO) {
        LOGGER.info("Registration received! {}", registrationRequestDTO);
    }

    @JmsListener(destination = "Login")
    public void loginListener(LoginRequestDTO loginRequestDTO) {
        LOGGER.info("Login received! {}", loginRequestDTO);
    }


}