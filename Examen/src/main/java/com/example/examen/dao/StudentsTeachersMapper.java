package com.example.examen.dao;

import com.example.examen.models.studentTeachersEntity;
import generate.StudentsTeachersEntityKey;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface StudentsTeachersMapper {

    List<Integer> allTeachersId(int id);

    List<Integer> allStudentsId(int id);

    void save(List<studentTeachersEntity> studentTeachersEntities);

    void remove(List<studentTeachersEntity> studentTeachersEntities);
}