package com.example.examen.dao;

import com.example.examen.dto.TeacherDTO;
import com.example.examen.models.TeachersEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TeachersMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TeachersEntity record);

    int insertSelective(TeachersEntity record);

    TeachersEntity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TeachersEntity record);

    int updateByPrimaryKey(TeachersEntity record);

    List<TeachersEntity> allTeachers();
}