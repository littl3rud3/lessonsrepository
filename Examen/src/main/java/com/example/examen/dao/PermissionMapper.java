package com.example.examen.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface PermissionMapper {

//    @Select("SELECT permission FROM permissions WHERE role_id = #{roleId}")
//    @Results(value = {
//            @Result(property = "permission", column = "permission", javaType = String.class)
//    })
    List<String> findPermissions(int roleId);
}
