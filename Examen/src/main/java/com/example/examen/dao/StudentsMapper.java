package com.example.examen.dao;

import com.example.examen.models.StudentsEntity;
import jdk.jfr.Registered;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface StudentsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(StudentsEntity record);

    int insertSelective(StudentsEntity record);

    StudentsEntity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(StudentsEntity record);

    int updateByPrimaryKey(StudentsEntity record);

    List<StudentsEntity> allStudents();
}