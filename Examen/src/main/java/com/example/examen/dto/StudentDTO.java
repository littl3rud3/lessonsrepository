package com.example.examen.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class StudentDTO implements Serializable {
    private Integer id;

    private String fullName;

    private String specialty;

    private String course;

//    private static final long serialVersionUID = 1L;
}
