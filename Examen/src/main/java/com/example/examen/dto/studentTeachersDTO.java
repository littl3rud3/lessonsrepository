package com.example.examen.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class studentTeachersDTO implements Serializable {
    private Integer idStudent;

    private Integer idTeacher;

//    private static final long serialVersionUID = 1L;
}
