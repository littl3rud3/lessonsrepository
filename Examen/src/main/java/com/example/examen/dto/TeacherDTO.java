package com.example.examen.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class TeacherDTO implements Serializable {
    private Integer id;

    private String fullName;

    private String department;

//    private static final long serialVersionUID = 1L;
}
