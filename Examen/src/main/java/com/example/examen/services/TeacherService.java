package com.example.examen.services;

import com.example.examen.dao.TeachersMapper;
import com.example.examen.dto.TeacherDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TeacherService {

    void edit(TeacherDTO teacherDTO);

    public List<TeacherDTO> allTeachers();

    public TeacherDTO findById(int id);

    public void addTeacher(TeacherDTO teacherDTO);

    public void deleteTeacher(int id);


}