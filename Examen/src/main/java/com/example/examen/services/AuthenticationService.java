package com.example.examen.services;

import com.example.examen.dto.LoginRequestDTO;
import com.example.examen.dto.RegistrationRequestDTO;
import com.example.examen.models.Account;
import org.springframework.security.core.Authentication;

public interface AuthenticationService {
    Authentication authorize(LoginRequestDTO loginRequestDTO);

    Account register(RegistrationRequestDTO registrationRequestDTO);

    void logout();

}
