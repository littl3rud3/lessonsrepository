package com.example.examen.services;

import com.example.examen.dto.StudentDTO;
import com.example.examen.dto.TeacherDTO;
import com.example.examen.dto.studentTeachersDTO;
import com.example.examen.models.studentTeachersEntity;
import generate.StudentsTeachersEntityKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public interface stService {

         void addNew(List<studentTeachersDTO> studentsTeachersDTOS);

         void remove(List<studentTeachersDTO> studentTeachersDTOS);

         List<TeacherDTO> allTeachers(int id);

         List<StudentDTO> allStudents(int id);

}
