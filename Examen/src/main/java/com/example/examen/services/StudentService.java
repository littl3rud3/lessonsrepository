package com.example.examen.services;

import com.example.examen.dao.StudentsMapper;
import com.example.examen.dto.StudentDTO;
import com.example.examen.models.StudentsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentService {

    void edit(StudentDTO studentDTO);

    public List<StudentDTO> allStudents();

    public StudentDTO findById(int id);

    public void addStudent(StudentDTO studentDTO);

    public void deleteStudent(int id);
}