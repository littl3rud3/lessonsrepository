CREATE table students(
                         id SERIAL PRIMARY KEY ,
                         full_name varchar(30) NOT NULL,
                         specialty varchar(30) NOT NULL,
                         course varchar(30) NOT NULL,
                         CHECK (length(full_name) > 5)

);