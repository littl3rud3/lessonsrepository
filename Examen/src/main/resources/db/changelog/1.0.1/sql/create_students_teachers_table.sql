CREATE table students_teachers(
                                  id_student int,
                                  id_teacher int,
                                  FOREIGN KEY (id_student) REFERENCES students (id) ON DELETE CASCADE,
                                  FOREIGN KEY (id_teacher) REFERENCES teachers (id) ON DELETE CASCADE,
                                  PRIMARY KEY (id_student, id_teacher)

);
