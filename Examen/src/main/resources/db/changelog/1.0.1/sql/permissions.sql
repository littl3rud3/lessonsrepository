CREATE TABLE if not exists permissions
(
        permission_id int,
        permission varchar(30),
        role_id int,
        PRIMARY KEY(permission_id, role_id),
    FOREIGN KEY (role_id) references roles
    );

