create sequence if not exists roles_seq start 1;

CREATE TABLE if not exists roles
(
    role_id  bigint      not null default nextval('roles_seq' :: regclass) primary key,
    role VARCHAR(50)
    );
ALTER TABLE account ADD FOREIGN KEY (role_id) references roles;
