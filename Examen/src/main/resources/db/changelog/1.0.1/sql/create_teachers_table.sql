CREATE table teachers(
                         id SERIAL PRIMARY KEY,
                         full_name varchar(30) NOT NULL ,
                         department varchar(30) NOT NULL,
                         CHECK (length(full_name) > 5)
);